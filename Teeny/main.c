#include <stdio.h>
#include <stdlib.h>



int main(int argc, char** argv) {

    int teeny;
    int *t;
    
    teeny=1;
    t=&teeny;
    
    printf("Variable teeny=%i\n", teeny);
    printf("Speicheradresse=%i\n", t);
    printf("Speicherinhalt=%i\n", *t);
    
    *t=64;
    
    printf("Variable teeny=%i\n", teeny);
    printf("Speicheradresse=%i\n", t);
    printf("Speicherinhalt=%i\n", *t);
    
    return (EXIT_SUCCESS);
}


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*Ratebereich*/
const int RANGE=100;
/*Anzahl Rateversuche*/
const int TRIES=6;

int rnd(int range);
void seedrnd(void);

int main() {

    int guessme;        
    int guess;
    int t; 
    
    seedrnd();/*Startwert für Zufallsgenerator*/
    guessme=rnd(RANGE);/*diese Zahl erraten*/
    
    printf("Rate!?!\nRate die Zufallszahl.\n");
    printf("Ich denke an eine Zahl zwischen 1 und %i.\n", RANGE);
    printf("Du hast %i Versuche. \n", TRIES);
    
    /*Geben wir also TRIES Versuche*/
    for(t=1; t<=6; t++)
    {
        printf("Versuch %i: ", t);
        scanf("%i", &guess);/*Rateversuch holen*/
        
        if(guess==guessme)/*richtig*/
        {
            printf("Du hast es erraten!\n");
            break;
        }
        else if (guess<guessme)
            printf("Zu niedrig!\n");
        else
            printf("Zu hoch!\n");
    }
    printf("Die Zahl war %i.\n", guessme);
    
    return (EXIT_SUCCESS);
}

/*
 Zufallszahlengenerator-Funktion
 liefert eine Zufallszahl wzischen 1 und Range 
 */

int rnd(int range)
{
    int r;
    
    r=rand()%range+1;
    return(r);
}

/*
Setzt Startwert für Zufallszahlen
*/

void seedrnd(void)
{
    srand((unsigned)time(NULL));
}
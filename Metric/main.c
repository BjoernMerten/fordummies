#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define EINGABELAENGE 20

int main(int argc, char** argv) {

    char input [EINGABELAENGE];
    double miles;
    double kilometers;
    double *m_adress;
    double *k_adress;
    double *i_adress;
    
    
    printf("Geben Sie einen Wert in Meilen ein: ");
    miles=atof(fgets(input, EINGABELAENGE, stdin));
    
    kilometers = miles * 1.609;
    
    printf("%.2f Meilen ergeben"
            "%.2f Kolometer.", miles, kilometers);
    
    m_adress=&miles;
    k_adress=&kilometers;
    i_adress=input;
    
    puts("\nVariablen:");
    printf("miles hat %i Bytes an Adresse%u\n",
            sizeof(miles), m_adress);
    printf("kilometres hat %i Bytes an Adresse%u\n",
            sizeof(kilometers), k_adress);
    printf("input hat %i Bytes an Adresse %u\n",
            sizeof(input), i_adress);
    
    return (EXIT_SUCCESS);
}


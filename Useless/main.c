#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {

    int i=1;
    int v=5;
    int c=10;
    int *address;
    
    address=&i;
    printf("Variable i=%u\n", *address);
    
    address=&v;
    printf("Variable v=%u\n", *address);
    
    address=&c;
    printf("Variable c=%u\n", *address);
            
    
    
    return (EXIT_SUCCESS);
}


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rnd(int bereich);
void seedrnd(void);

int main(int argc, char** argv) 
{

    //seedrnd();
    
    for(int x=0;x<100;x++)
        printf("%i\t", rnd(6));
    
    return (0);
}


int rnd(int bereich)
{
    int r;
    
    r=rand()%bereich+1;
    return r;
}

void seedrnd(void)
{
    srand((unsigned)time(NULL));
}
